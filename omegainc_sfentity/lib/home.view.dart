import 'package:flutter/material.dart';
import 'package:omegainc_sfentity/model/model.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {

  Future<void> create() async {
    try {
        final result = await Cad_pessoa(
          nome: 'Jefferson',
          cpf: 09543,
          datacadastro: new DateTime.now(),
          email: 'tico254@gmail.com',
          telefone: '44999054394',
          idpessoa: '123'
        ).save();

        print(result);
    } catch (error) {
      print(error);
    }
  }

  Future<void> createEndereco() async {
    try {
      Cad_pessoa jefferson = await Cad_pessoa().getById(1);


      if (jefferson != null) {
        final id = await Cad_pessoaendereco(
          bairro: 'Jardim Novo Horizonte',
          cep: '87303034',
          cidade: 'Campo Mourão',
          endereco: 'Rua Santa Cruz',
          estado: 'PR',
          pais: 'Brasil'
        ).save();

        jefferson.idendereco = id;
        await jefferson.save();
      }

    } catch (error) {
      print(error);
    }
  }

  Future<void> get() async {
    try {
      Cad_pessoa jefferson = await Cad_pessoa().getById(1);

      if (jefferson != null) {
        Cad_pessoaendereco endereco = await jefferson.getCad_pessoaendereco();
        print(endereco);
      }

    } catch (error) {
      print(error);
    }
  }

  Future<void> select() async {
    try {
      List<Cad_pessoa> result = await Cad_pessoa().select().toList();

      List<Cad_pessoa> result2 = await Cad_pessoa()
          .select()
          .nome.contains('jefferson')
          .toList();

      result.forEach((element) {
        print(element.nome);
      });

      final resultONE = result.where((element) => element.nome == 'Jefferson');

      print(resultONE);

    } catch (error) {
      print(error);
    }
  }

  Future<void> update() async {
    try {
      Cad_pessoa jefferson = await Cad_pessoa()
                                .select()
                                .id.equals(1)
                                .toSingle();

      if (jefferson != null) {
        jefferson.nome = jefferson.nome + ' Atualizado';

        jefferson.save();
      }

      print(jefferson);
    } catch (error) {
      print(error);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('SQFEntity Tests'),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(5.0),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: FlatButton(
                    child: Text('Test insert'),
                    onPressed: () {
                      this.create();
                    },
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: FlatButton(
                    child: Text('Test insert endereco'),
                    onPressed: () {
                      this.createEndereco();
                    },
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: FlatButton(
                    child: Text('Test update'),
                    onPressed: () {
                      this.update();
                    },
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: FlatButton(
                    child: Text('Test delete'),
                    onPressed: () {

                    },
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: FlatButton(
                    child: Text('Test select'),
                    onPressed: () {
                      this.select();
                    },
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: FlatButton(
                    child: Text('Test select one'),
                    onPressed: () {
                      this.get();
                    },
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

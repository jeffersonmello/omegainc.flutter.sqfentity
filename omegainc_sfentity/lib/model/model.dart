import 'dart:convert';
import 'dart:typed_data';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:sqfentity/sqfentity.dart';
import 'package:sqfentity_gen/sqfentity_gen.dart';

part 'model.g.dart';

const tablePessoa = SqfEntityTable(
    tableName: 'cad_pessoa',
    primaryKeyName: 'id',
    primaryKeyType: PrimaryKeyType.integer_auto_incremental,
    useSoftDeleting: true,
    modelName: null,
    fields: [
      SqfEntityField('idpessoa', DbType.text),
      SqfEntityField('nome', DbType.text),
      SqfEntityField('email', DbType.text),
      SqfEntityField('telefone', DbType.text),
      SqfEntityField('cpf', DbType.integer),
      SqfEntityField('datacadastro', DbType.datetime),
      SqfEntityField('umnovocampo', DbType.integer),
      SqfEntityField('umnovocampo2', DbType.integer),
      SqfEntityFieldRelationship(
        parentTable: tableEndereco,
        deleteRule: DeleteRule.NO_ACTION,
        fieldName: 'idendereco',

      ),
    ],
);

const tableEndereco = SqfEntityTable(
    tableName: 'cad_pessoaendereco',
    primaryKeyName: 'idendereco',
    primaryKeyType: PrimaryKeyType.integer_auto_incremental,
    useSoftDeleting: true,
    modelName: null,
    fields: [
      SqfEntityField('cep', DbType.text),
      SqfEntityField('endereco', DbType.text),
      SqfEntityField('bairro', DbType.text),
      SqfEntityField('cidade', DbType.text),
      SqfEntityField('estado', DbType.text),
      SqfEntityField('pais', DbType.text),
    ]
);

const seqIdentity = SqfEntitySequence(
  sequenceName: 'identity',
);

@SqfEntityBuilder(myDbModel)
const myDbModel = SqfEntityModel(
    modelName: 'TestModel', // optional
    databaseName: 'testdb.db',
    databaseTables: [
      tablePessoa,
      tableEndereco
    ],
    sequences: [seqIdentity],
    bundledDatabasePath: null
);